package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Car extends Vehicle {
    public boolean isSportCar;
    public boolean isElectricCar;

    public Car(int year, String color, boolean sport, boolean electric) {
        super(year, color);
        setCarType(sport, electric);
    }

    public Car(int year, VehicleColor color, boolean sport, boolean electric) {
        super(year, color);
        setCarType(sport, electric);
    }

    @Override
    public String getVehicleInfo() {
        return String.format("VIN: %1s\nProduction year: %2s\nColor: %3s\nIs it sport car?: %1b\nIs car electric?: %2b",
                this.vin,
                this.yearOfProduction,
                this.color,
                this.isSportCar,
                this.isElectricCar
        );
    }

    public void setCarType(boolean sport, boolean electric) {
        this.isElectricCar = electric;
        this.isSportCar = sport;
    }

    public boolean isElectricCar() {
        return this.isElectricCar;
    }

    public boolean isSportCar() {
        return this.isSportCar;
    }
}
