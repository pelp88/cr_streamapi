package ru.codeinside.lessons.javacore.inheritance;

public class VehicleDemo {
    public static void main(String[] args) {
        Car car = new Car(2010, "light green", true, false);
        Truck truck = new Truck(1992, "cyan", false);

        System.out.println(car.getVehicleInfo());
        System.out.println(truck.getVehicleInfo());
    }
}
